/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BTN_PROGRAM_Pin GPIO_PIN_13
#define BTN_PROGRAM_GPIO_Port GPIOC
#define HALL1_STOP_Pin GPIO_PIN_0
#define HALL1_STOP_GPIO_Port GPIOC
#define HALL2_STOP_Pin GPIO_PIN_1
#define HALL2_STOP_GPIO_Port GPIOC
#define COIL1_ORANGE_Pin GPIO_PIN_0
#define COIL1_ORANGE_GPIO_Port GPIOA
#define COIL2_YELLOW_Pin GPIO_PIN_1
#define COIL2_YELLOW_GPIO_Port GPIOA
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define COIL3_PINK_Pin GPIO_PIN_4
#define COIL3_PINK_GPIO_Port GPIOA
#define LED_END_SENSOR_HALL_Pin GPIO_PIN_5
#define LED_END_SENSOR_HALL_GPIO_Port GPIOA
#define PWM_RED_03_Pin GPIO_PIN_6
#define PWM_RED_03_GPIO_Port GPIOA
#define PWM_GREEN_03_Pin GPIO_PIN_7
#define PWM_GREEN_03_GPIO_Port GPIOA
#define UART3_TX_DEBUG_Pin GPIO_PIN_4
#define UART3_TX_DEBUG_GPIO_Port GPIOC
#define UART3_RX_DEBUG_Pin GPIO_PIN_5
#define UART3_RX_DEBUG_GPIO_Port GPIOC
#define COIL4_BLUE_Pin GPIO_PIN_0
#define COIL4_BLUE_GPIO_Port GPIOB
#define PWM_BLUE_03_Pin GPIO_PIN_1
#define PWM_BLUE_03_GPIO_Port GPIOB
#define IN1_SERVICE_Pin GPIO_PIN_2
#define IN1_SERVICE_GPIO_Port GPIOB
#define PWM_GREEN_02_Pin GPIO_PIN_10
#define PWM_GREEN_02_GPIO_Port GPIOB
#define PWM_BLUE_02_Pin GPIO_PIN_11
#define PWM_BLUE_02_GPIO_Port GPIOB
#define LED4_SERVICE_Pin GPIO_PIN_6
#define LED4_SERVICE_GPIO_Port GPIOC
#define LED_POWER_Pin GPIO_PIN_7
#define LED_POWER_GPIO_Port GPIOC
#define PWM_BUZ_Pin GPIO_PIN_8
#define PWM_BUZ_GPIO_Port GPIOC
#define LED3_SERVICE_Pin GPIO_PIN_9
#define LED3_SERVICE_GPIO_Port GPIOC
#define PWM_RED_01_Pin GPIO_PIN_8
#define PWM_RED_01_GPIO_Port GPIOA
#define PWM_GREEN_01_Pin GPIO_PIN_9
#define PWM_GREEN_01_GPIO_Port GPIOA
#define PWM_BLUE_01_Pin GPIO_PIN_10
#define PWM_BLUE_01_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define PWM_RED_02_Pin GPIO_PIN_15
#define PWM_RED_02_GPIO_Port GPIOA
#define IN3_SERVICE_Pin GPIO_PIN_10
#define IN3_SERVICE_GPIO_Port GPIOC
#define IN2_SERVICE_Pin GPIO_PIN_11
#define IN2_SERVICE_GPIO_Port GPIOC
#define ACC_IN_Pin GPIO_PIN_12
#define ACC_IN_GPIO_Port GPIOC
#define BTN_START_STOP_Pin GPIO_PIN_2
#define BTN_START_STOP_GPIO_Port GPIOD
#define PWM1_SERVICE_Pin GPIO_PIN_3
#define PWM1_SERVICE_GPIO_Port GPIOB
#define LED_STATUS_Pin GPIO_PIN_4
#define LED_STATUS_GPIO_Port GPIOB
#define LED_UV_ON_OFF_Pin GPIO_PIN_5
#define LED_UV_ON_OFF_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
