/**************************************************************************/
/*!
    @file     Adafruit_ADXL345_U.cpp
    @author   K.Townsend (Adafruit Industries)
*/
/**************************************************************************/


#include "ADXL345.h"
#include "globals.h"
#include <math.h>
#include "i2c.h"
#include "iwdg.h"
#include "string.h"
/**************************************************************************/
/*!
    @brief  And abstract 'send' method for different versions of the Arduino
   Wire library
    @returns The byte read
*/
/**************************************************************************/


/**************************************************************************/
/*!
    @brief  And abstract 'send' method for different versions of the Arduino
   Wire library
    @param x The byte to send
*/
/**************************************************************************/


/**************************************************************************/

/**
 * @brief  Abstract away SPI receiver & transmitter

 * @param clock The pin number for SCK, the SPI ClocK line
 * @param miso The pin number for MISO, the SPI Master In Slave Out line
 * @param mosi The pin number for MOSI, the SPI Master Out Slave In line
 * @param data The byte to send
 *
 * @return uint8_t The single byte response
 */
//static uint8_t spixfer(uint8_t clock, uint8_t miso, uint8_t mosi,
//                       uint8_t data) {
//  uint8_t reply = 0;
//  for (int i = 7; i >= 0; i--) {
//    reply <<= 1;
//    digitalWrite(clock, LOW);
//    digitalWrite(mosi, data & (1 << i));
//    digitalWrite(clock, HIGH);
//    if (digitalRead(miso))
//      reply |= 1;
//  }
//  return reply;
//}

/**************************************************************************/
/*!
    @brief  Writes one byte to the specified destination register
    @param reg The address of the register to write to
    @param value The value to set the register to
*/
/**************************************************************************/
void ADXL345_writeRegister(uint8_t reg, uint8_t value) {

//    Wire.beginTransmission((uint8_t)_i2caddr);
//    i2cwrite((uint8_t)reg);
//    i2cwrite((uint8_t)(value));
//    Wire.endTransmission();
     uint8_t buff[2];
   	 //Load the register address and 8-bit data
   	 buff[0] = reg;
   	 buff[1] = value;
   	 HAL_I2C_Master_Transmit(&hi2c1,ADXL345_sens.i2caddr<<1,(uint8_t *)buff,2,1000);
}

/**************************************************************************/
/*!
    @brief Reads one byte from the specified register
    @param reg The address of the register to read from
    @returns The single byte value of the requested register
*/
/**************************************************************************/
uint8_t ADXL345_readRegister(uint8_t reg) {

//    Wire.beginTransmission((uint8_t)_i2caddr);
//    i2cwrite(reg);
//    Wire.endTransmission();
//    Wire.requestFrom((uint8_t)_i2caddr, 1);
//    return (i2cread());
    uint8_t value[1];
	uint8_t adr_rd=(ADXL345_sens.i2caddr<<1);
	adr_rd =(adr_rd|0x01);
	//Wire.beginTransmission(L3G4200D_ADDRESS);
	value[0]=reg;

	HAL_I2C_Master_Transmit(&hi2c1,ADXL345_sens.i2caddr<<1,(uint8_t *)value,1,1000);//Wire.send(reg);

	HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)value,1,1000);//Wire.requestFrom(L3G4200D_ADDRESS, 1);

	return value[0];

}

/**************************************************************************/
/*!
    @brief Reads two bytes from the specified register
    @param reg The address of the register to read from
    @return The two bytes read from the sensor starting at the given address
*/
/**************************************************************************/
int16_t ADXL345_read16(uint8_t reg) {

//    Wire.beginTransmission((uint8_t)_i2caddr);
//    i2cwrite(reg);
//    Wire.endTransmission();
//    Wire.requestFrom((uint8_t)_i2caddr, 2);
//    return (uint16_t)(i2cread() | (i2cread() << 8));

    uint8_t value[2];
    int16_t result;
   	uint8_t adr_rd=(ADXL345_sens.i2caddr<<1);
   	adr_rd =(adr_rd|0x01);
   	//Wire.beginTransmission(L3G4200D_ADDRESS);
   	value[0]=reg;
   	HAL_I2C_Master_Transmit(&hi2c1,ADXL345_sens.i2caddr<<1,(uint8_t *)value,1,1000);//Wire.send(reg);
   	HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)value,2,1000);//Wire.requestFrom(L3G4200D_ADDRESS, 1);
    result= (int16_t)((value[1] << 8)|value[0]);
   	return (result);

}

/**************************************************************************/
/*!
    @brief  Reads the device ID (can be used to check connection)
    @return The Device ID of the connected sensor
*/
/**************************************************************************/
uint8_t ADXL345_getDeviceID(void) {
  // Check device ID register
  return ADXL345_readRegister(ADXL345_REG_DEVID);
}

/**************************************************************************/
/*!
    @brief  Gets the most recent X axis value
    @return The raw `int16_t` unscaled x-axis acceleration value
*/
/**************************************************************************/
int16_t ADXL345_getX(void) {
  return ADXL345_read16(ADXL345_REG_DATAX0);
}

/**************************************************************************/
/*!
    @brief  Gets the most recent Y axis value
    @return The raw `int16_t` unscaled y-axis acceleration value
*/
/**************************************************************************/
int16_t ADXL345_getY(void) {
  return ADXL345_read16(ADXL345_REG_DATAY0);
}

/**************************************************************************/
/*!
    @brief  Gets the most recent Z axis value
    @return The raw `int16_t` unscaled z-axis acceleration value
*/
/**************************************************************************/
int16_t ADXL345_getZ(void) {
  return ADXL345_read16(ADXL345_REG_DATAZ0);
}

/**************************************************************************/
/*!
    @brief  Instantiates a new ADXL345 class
    @param sensorID A unique ID to use to differentiate the sensor from others
*/
/**************************************************************************/
void ADXL345_Init(void) {
  ADXL345_sens.sensorID =12345;
  ADXL345_sens.range = ADXL345_RANGE_2_G;
  ADXL345_sens.i2c = true;
  ADXL345_sens.pitch_th=12.0f;
  ADXL345_sens.roll_th=8.0f;
  ADXL345_sens.yaw_th=0.0f;
  ADXL345_sens.acc_th=3.5f;

  ADXL345_getSensor();
  //ADXL345_begin(ADXL345_DEFAULT_ADDRESS);
  /* Initialise the sensor */
    if(! ADXL345_begin(ADXL345_DEFAULT_ADDRESS))
    {
		  /* There was a problem detecting the ADXL345 ... check your connections */
		  printf("No ADXL345 detected ... Check your wiring!");
		  ADXL345_sens.sensorID=0;
    }else{
		/* Set the range to whatever is appropriate for your project */
		//ADXL345_setRange(ADXL345_RANGE_16_G);
		//ADXL345_setRange(ADXL345_RANGE_8_G);
		//ADXL345_setRange(ADXL345_RANGE_4_G);
		ADXL345_setRange(ADXL345_RANGE_2_G);
		/*Calibrate Offset*/
		ADXL345_writeRegister(ADXL345_REG_OFSX, 1);//Offset for Z-axis
		ADXL345_writeRegister(ADXL345_REG_OFSZ, 5);//Offset for Z-axis
		 // Enable measurement mode (0b00001000)
		ADXL345_writeRegister(ADXL345_REG_POWER_CTL, 0x08);

		// Values for Activity and Inactivity detection
		ADXL345_setActivityThreshold(1.5);    // Recommended 2 g
		//ADXL345_setInactivityThreshold(2.0);  // Recommended 2 g
		//ADXL345_setTimeInactivity(1);         // Recommended 5 s

		// Set activity detection only on X,Y,Z-Axis
		//ADXL345_setActivityXYZ(1);         // Check activity on X,Y,Z-Axis
		  // or
	    ADXL345_setActivityX(1);        // Check activity on X_Axis
		// ADXL345_setActivityY(1);        // Check activity on Y-Axis
		// ADXL345_setActivityZ(1);        // Check activity on Z-Axis

		// Set inactivity detection only on X,Y,Z-Axis
		// ADXL345_setInactivityXYZ(1);       // Check inactivity on X,Y,Z-Axis
		// or
		// ADXL345_setInactivityX(1);      // Check inactivity on X_Axis
		// ADXL345_setInactivityY(1);      // Check inactivity on Y-Axis
		// ADXL345_setInactivityZ(1);      // Check inactivity on Z-Axis

	    // Select INT 1 for get activities
	    ADXL345_useInterrupt(ADXL345_INT1);

		//ADXL345_clearSettings();


		/* Display some basic information on this sensor */
		ADXL345_displaySensorDetails();
		/* Display additional settings (outside the scope of sensor_t) */
		ADXL345_displayDataRate();
		ADXL345_displayRange();
    }

}

/**************************************************************************/
/*!
    @brief  Instantiates a new ADXL345 class in SPI mode
    @param clock The pin number for SCK, the SPI ClocK line
    @param miso The pin number for MISO, the SPI Master In Slave Out line
    @param mosi The pin number for MOSI, the SPI Master Out Slave In line
    @param cs The pin number for CS, the SPI Chip Select line
    @param sensorID A unique ID to use to differentiate the sensor from others
*/
/**************************************************************************/
//void ADXL345_SPI(uint8_t clock, uint8_t miso,uint8_t mosi, uint8_t cs,int32_t sensorID) {
//  _sensorID = sensorID;
//  _range = ADXL345_RANGE_2_G;
//  _cs = cs;
//  _clk = clock;
//  _do = mosi;
//  _di = miso;
//  _i2c = false;
//}

/**************************************************************************/
/*!
    @brief  Setups the HW (reads coefficients values, etc.)
    @param i2caddr The I2C address to begin communication with
    @return true: success false: a sensor with the correct ID was not found
*/
/**************************************************************************/
bool ADXL345_begin(uint8_t i2caddr) {
	ADXL345_sens.i2caddr = i2caddr;
   // Wire.begin();
  /* Check connection */
  uint8_t deviceid = ADXL345_getDeviceID();
  if (deviceid != 0xE5) {
    /* No ADXL345 detected ... return false */
    return false;
  }

  // Enable measurements
  ADXL345_writeRegister(ADXL345_REG_POWER_CTL, 0x08);

  return true;
}

/**************************************************************************/
/*!
    @brief  Sets the g range for the accelerometer
    @param range The new `range_t` to set the accelerometer to
*/
/**************************************************************************/
void ADXL345_setRange(range_t range) {
  /* Read the data format register to preserve bits */
  uint8_t format = ADXL345_readRegister(ADXL345_REG_DATA_FORMAT);

  /* Update the data rate */
  format &= ~0x0F;
  format |= range;

  /* Make sure that the FULL-RES bit is enabled for range scaling */
  format |= 0x08;

  /* Write the register back to the IC */
  ADXL345_writeRegister(ADXL345_REG_DATA_FORMAT, format);

  /* Keep track of the current range (to avoid readbacks) */
  ADXL345_sens.range = range;
}

/**************************************************************************/
/*!
    @brief  Gets the g range for the accelerometer
    @return The current `range_t` value
*/
/**************************************************************************/
range_t ADXL345_getRange(void) {
  /* Read the data format register to preserve bits */
  return (range_t)(ADXL345_readRegister(ADXL345_REG_DATA_FORMAT) & 0x03);
}

/**************************************************************************/
/*!
    @brief  Sets the data rate for the ADXL345 (controls power consumption)
    @param dataRate The `dataRate_t` to set
*/
/**************************************************************************/
void ADXL345_setDataRate(dataRate_t dataRate) {
  /* Note: The LOW_POWER bits are currently ignored and we always keep
     the device in 'normal' mode */
	ADXL345_writeRegister(ADXL345_REG_BW_RATE, dataRate);
}

/**************************************************************************/
/*!
    @brief  Gets the data rate for the ADXL345 (controls power consumption)
    @return The current data rate
*/
/**************************************************************************/
dataRate_t ADXL345_getDataRate(void) {
  return (dataRate_t)(ADXL345_readRegister(ADXL345_REG_BW_RATE) & 0x0F);
}

/**************************************************************************/
/*!
    @brief  Gets the most recent sensor event
    @param event Pointer to the event object to fill
    @return true: success
*/
/**************************************************************************/
bool ADXL345_getEvent(void) {
  /* Clear the event */
  //memset(event, 0, sizeof(sensors_event_t));

	if(ADXL345_sens.sensorID!=0){
		ADXL345_sens.timestamp = 0;
		ADXL345_sens.acc_x = ADXL345_getX() * ADXL345_MG2G_MULTIPLIER ;
		ADXL345_sens.acc_y = ADXL345_getY() * ADXL345_MG2G_MULTIPLIER ;
		ADXL345_sens.acc_z = ADXL345_getZ() * ADXL345_MG2G_MULTIPLIER ;
		ADXL345_sens.roll  = (atan2(   ADXL345_sens.acc_y, sqrt((ADXL345_sens.acc_x*ADXL345_sens.acc_x)  + (ADXL345_sens.acc_z*ADXL345_sens.acc_z)))*180.0)/M_PI;
		ADXL345_sens.pitch = (atan2(-1*ADXL345_sens.acc_x, sqrt((ADXL345_sens.acc_y*ADXL345_sens.acc_y)  + (ADXL345_sens.acc_z*ADXL345_sens.acc_z)))*180.0)/M_PI;
		ADXL345_sens.yaw   = (atan2(   ADXL345_sens.acc_z, sqrt((ADXL345_sens.acc_x*ADXL345_sens.acc_x)  + (ADXL345_sens.acc_y*ADXL345_sens.acc_y)))*180.0)/M_PI;
		ADXL345_sens.acc_x = ADXL345_sens.acc_x * SENSORS_GRAVITY_STANDARD;
		ADXL345_sens.acc_y = ADXL345_sens.acc_y * SENSORS_GRAVITY_STANDARD;
		ADXL345_sens.acc_z = ADXL345_sens.acc_z* SENSORS_GRAVITY_STANDARD;
		// Calculate Pitch & Roll (Low Pass Filter)
		ADXL345_sens.fpitch = ((0.94*ADXL345_sens.fpitch)+(ADXL345_sens.pitch*0.06));//-(atan2(filtered.XAxis, sqrt(filtered.YAxis*filtered.YAxis + filtered.ZAxis*filtered.ZAxis))*180.0)/M_PI;
		ADXL345_sens.froll  = ((0.94*ADXL345_sens.froll)+(ADXL345_sens.roll*0.06));//(atan2(filtered.YAxis, filtered.ZAxis)*180.0)/M_PI;
	}
	return true;
}

/**************************************************************************/
/*!
 */
/**************************************************************************/

/**
 * @brief Fill a `sensor_t` struct with information about the sensor
 *
 * @param sensor Pointer to a `sensor_t` struct to fill
 */
void ADXL345_getSensor(void) {
  /* Clear the sensor_t object */
  // memset(sensor, 0, sizeof(sensor_t));
  /* Insert the sensor name in the fixed length char array */
  strncpy(ADXL345_sens.name, "ADXL345", sizeof(ADXL345_sens.name) - 1);
  ADXL345_sens.name[sizeof(ADXL345_sens.name) - 1] = 0;
  ADXL345_sens.version    = 1;
  ADXL345_sens.type       = SENSOR_TYPE_ACCELEROMETER;
  ADXL345_sens.min_delay  = 0;
  ADXL345_sens.max_value  = -156.9064F; /* -16g = 156.9064 m/s^2  */
  ADXL345_sens.min_value  = 156.9064F;  /*  16g = 156.9064 m/s^2  */
  ADXL345_sens.resolution = 0.03923F;  /*  4mg = 0.0392266 m/s^2 */
}




// Low Pass Filter
Vector ADXL345_lowPassFilter(Vector vector, float alpha)
{
	ADXL345_sens.f.XAxis = vector.XAxis * alpha + (ADXL345_sens.f.XAxis * (1.0 - alpha));
	ADXL345_sens.f.YAxis = vector.YAxis * alpha + (ADXL345_sens.f.YAxis * (1.0 - alpha));
	ADXL345_sens.f.ZAxis = vector.ZAxis * alpha + (ADXL345_sens.f.ZAxis * (1.0 - alpha));
    return ADXL345_sens.f;
}

// Read raw values
Vector ADXL345_readRaw(void)
{
	ADXL345_sens.r.XAxis = ADXL345_read16(ADXL345_REG_DATAX0);
	ADXL345_sens.r.YAxis = ADXL345_read16(ADXL345_REG_DATAY0);
	ADXL345_sens.r.ZAxis = ADXL345_read16(ADXL345_REG_DATAZ0);
    return ADXL345_sens.r;
}

// Read normalized values
Vector ADXL345_readNormalize(float gravityFactor)
{
	ADXL345_readRaw();

    // (4 mg/LSB scale factor in Full Res) * gravity factor
    ADXL345_sens.n.XAxis = ADXL345_sens.r.XAxis * 0.004 * gravityFactor;
    ADXL345_sens.n.YAxis = ADXL345_sens.r.YAxis * 0.004 * gravityFactor;
    ADXL345_sens.n.ZAxis = ADXL345_sens.r.ZAxis * 0.004 * gravityFactor;

    return ADXL345_sens.n;
}

// Read scaled values
Vector ADXL345_readScaled(void)
{
	ADXL345_readRaw();

    // (4 mg/LSB scale factor in Full Res)
    ADXL345_sens.n.XAxis = ADXL345_sens.r.XAxis * 0.004;
    ADXL345_sens.n.YAxis = ADXL345_sens.r.YAxis * 0.004;
    ADXL345_sens.n.ZAxis = ADXL345_sens.r.ZAxis * 0.004;

    return ADXL345_sens.n;
}

void ADXL345_clearSettings(void)
{
	ADXL345_setRange(ADXL345_RANGE_2_G);
	ADXL345_setDataRate(ADXL345_DATARATE_100_HZ);

    ADXL345_writeRegister(ADXL345_REG_THRESH_TAP, 0x00);
    ADXL345_writeRegister(ADXL345_REG_DUR, 0x00);
    ADXL345_writeRegister(ADXL345_REG_LATENT, 0x00);
    ADXL345_writeRegister(ADXL345_REG_WINDOW, 0x00);
    ADXL345_writeRegister(ADXL345_REG_THRESH_ACT, 0x00);
    ADXL345_writeRegister(ADXL345_REG_THRESH_INACT, 0x00);
    ADXL345_writeRegister(ADXL345_REG_TIME_INACT, 0x00);
    ADXL345_writeRegister(ADXL345_REG_THRESH_FF, 0x00);
    ADXL345_writeRegister(ADXL345_REG_TIME_FF, 0x00);

    uint8_t value;

    value = ADXL345_readRegister(ADXL345_REG_ACT_INACT_CTL);
    value &= 0b10001000;
    ADXL345_writeRegister(ADXL345_REG_ACT_INACT_CTL, value);

    value = ADXL345_readRegister(ADXL345_REG_TAP_AXES);
    value &= 0b11111000;
    ADXL345_writeRegister(ADXL345_REG_TAP_AXES, value);
}

// Set Tap Threshold (62.5mg / LSB)
void ADXL345_setTapThreshold(float threshold)
{
    float scaled = (threshold / 0.0625f);
    ADXL345_writeRegister(ADXL345_REG_THRESH_TAP, (uint8_t)scaled);
}

// Get Tap Threshold (62.5mg / LSB)
float ADXL345_getTapThreshold(void)
{
    return ADXL345_readRegister(ADXL345_REG_THRESH_TAP) * 0.0625f;
}

// Set Tap Duration (625us / LSB)
void ADXL345_setTapDuration(float duration)
{
   float scaled = (duration / 0.000625f);
    ADXL345_writeRegister(ADXL345_REG_DUR, (uint8_t)scaled);
}

// Get Tap Duration (625us / LSB)
float ADXL345_getTapDuration(void)
{
    return ADXL345_readRegister(ADXL345_REG_DUR) * 0.000625f;
}

// Set Double Tap Latency (1.25ms / LSB)
void ADXL345_setDoubleTapLatency(float latency)
{
    float scaled = (latency / 0.00125f);
    ADXL345_writeRegister(ADXL345_REG_LATENT, (uint8_t)scaled);
}

// Get Double Tap Latency (1.25ms / LSB)
float ADXL345_getDoubleTapLatency()
{
    return ADXL345_readRegister(ADXL345_REG_LATENT) * 0.00125f;
}

// Set Double Tap Window (1.25ms / LSB)
void ADXL345_setDoubleTapWindow(float window)
{
    float scaled = (window / 0.00125f);
    ADXL345_writeRegister(ADXL345_REG_WINDOW,(uint8_t) scaled);
}

// Get Double Tap Window (1.25ms / LSB)
float ADXL345_getDoubleTapWindow(void)
{
    return ADXL345_readRegister(ADXL345_REG_WINDOW) * 0.00125f;
}

// Set Activity Threshold (62.5mg / LSB)
void ADXL345_setActivityThreshold(float threshold)
{
    float scaled = (threshold / 0.0625f);
    printf("Activity Ths:%d\n\r",(uint8_t)scaled);
    ADXL345_writeRegister(ADXL345_REG_THRESH_ACT,(uint8_t) scaled);
}

// Get Activity Threshold (65.5mg / LSB)
float ADXL345_getActivityThreshold(void)
{
    return ADXL345_readRegister(ADXL345_REG_THRESH_ACT) * 0.0625f;
}

// Set Inactivity Threshold (65.5mg / LSB)
void ADXL345_setInactivityThreshold(float threshold)
{
    float scaled = (threshold / 0.0625f);
    ADXL345_writeRegister(ADXL345_REG_THRESH_INACT, scaled);
}

// Get Incactivity Threshold (65.5mg / LSB)
float ADXL345_getInactivityThreshold(void)
{
    return ADXL345_readRegister(ADXL345_REG_THRESH_INACT) * 0.0625f;
}

// Set Inactivity Time (s / LSB)
void ADXL345_setTimeInactivity(uint8_t time)
{
    ADXL345_writeRegister(ADXL345_REG_TIME_INACT, time);
}

// Get Inactivity Time (s / LSB)
uint8_t ADXL345_getTimeInactivity(void)
{
    return ADXL345_readRegister(ADXL345_REG_TIME_INACT);
}

// Set Free Fall Threshold (65.5mg / LSB)
void ADXL345_setFreeFallThreshold(float threshold)
{
    float scaled = (threshold / 0.0625f);
    ADXL345_writeRegister(ADXL345_REG_THRESH_FF, (uint8_t)scaled);
}

// Get Free Fall Threshold (65.5mg / LSB)
float ADXL345_getFreeFallThreshold(void)
{
    return ADXL345_readRegister(ADXL345_REG_THRESH_FF) * 0.0625f;
}

// Set Free Fall Duratiom (5ms / LSB)
void ADXL345_setFreeFallDuration(float duration)
{
    float scaled = (duration / 0.005f);
    ADXL345_writeRegister(ADXL345_REG_TIME_FF, (uint8_t)scaled);
}

// Get Free Fall Duratiom
float ADXL345_getFreeFallDuration()
{
    return ADXL345_readRegister(ADXL345_REG_TIME_FF) * 0.005f;
}

void ADXL345_setActivityX(bool state)
{
	ADXL345_writeRegisterBit(ADXL345_REG_ACT_INACT_CTL, 6, state);
}

bool ADXL345_getActivityX(void)
{
    return ADXL345_readRegisterBit(ADXL345_REG_ACT_INACT_CTL, 6);
}

void ADXL345_setActivityY(bool state)
{
	ADXL345_writeRegisterBit(ADXL345_REG_ACT_INACT_CTL, 5, state);
}

bool ADXL345_getActivityY(void)
{
    return ADXL345_readRegisterBit(ADXL345_REG_ACT_INACT_CTL, 5);
}

void ADXL345_setActivityZ(bool state)
{
	ADXL345_writeRegisterBit(ADXL345_REG_ACT_INACT_CTL, 4, state);
}

bool ADXL345_getActivityZ(void)
{
    return ADXL345_readRegisterBit(ADXL345_REG_ACT_INACT_CTL, 4);
}

void ADXL345_setActivityXYZ(bool state)
{
    uint8_t value;

    value = ADXL345_readRegister(ADXL345_REG_ACT_INACT_CTL);

    if (state)
    {
	value |= 0b00111000;
    } else
    {
	value &= 0b11000111;
    }

    ADXL345_writeRegister(ADXL345_REG_ACT_INACT_CTL, value);
}


void ADXL345_setInactivityX(bool state)
{
	ADXL345_writeRegisterBit(ADXL345_REG_ACT_INACT_CTL, 2, state);
}

bool ADXL345_getInactivityX(void)
{
    return ADXL345_readRegisterBit(ADXL345_REG_ACT_INACT_CTL, 2);
}

void ADXL345_setInactivityY(bool state)
{
	ADXL345_writeRegisterBit(ADXL345_REG_ACT_INACT_CTL, 1, state);
}

bool ADXL345_getInactivityY(void)
{
    return ADXL345_readRegisterBit(ADXL345_REG_ACT_INACT_CTL, 1);
}

void ADXL345_setInactivityZ(bool state)
{
	ADXL345_writeRegisterBit(ADXL345_REG_ACT_INACT_CTL, 0, state);
}

bool ADXL345_getInactivityZ(void)
{
    return ADXL345_readRegisterBit(ADXL345_REG_ACT_INACT_CTL, 0);
}

void ADXL345_setInactivityXYZ(bool state)
{
    uint8_t value;

    value = ADXL345_readRegister(ADXL345_REG_ACT_INACT_CTL);

    if (state)
    {
	value |= 0b00000111;
    } else
    {
	value &= 0b11111000;
    }

    ADXL345_writeRegister(ADXL345_REG_ACT_INACT_CTL, value);
}

void ADXL345_setTapDetectionX(bool state)
{
	ADXL345_writeRegisterBit(ADXL345_REG_TAP_AXES, 2, state);
}

bool ADXL345_getTapDetectionX(void)
{
    return ADXL345_readRegisterBit(ADXL345_REG_TAP_AXES, 2);
}

void ADXL345_setTapDetectionY(bool state)
{
	ADXL345_writeRegisterBit(ADXL345_REG_TAP_AXES, 1, state);
}

bool ADXL345_getTapDetectionY(void)
{
    return ADXL345_readRegisterBit(ADXL345_REG_TAP_AXES, 1);
}

void ADXL345_setTapDetectionZ(bool state)
{
	ADXL345_writeRegisterBit(ADXL345_REG_TAP_AXES, 0, state);
}

bool ADXL345_getTapDetectionZ(void)
{
    return ADXL345_readRegisterBit(ADXL345_REG_TAP_AXES, 0);
}

void ADXL345_setTapDetectionXYZ(bool state)
{
    uint8_t value;

    value = ADXL345_readRegister(ADXL345_REG_TAP_AXES);

    if (state)
    {
	value |= 0b00000111;
    } else
    {
	value &= 0b11111000;
    }

    ADXL345_writeRegister(ADXL345_REG_TAP_AXES, value);
}


void ADXL345_useInterrupt(adxl345_int_t interrupt)
{
    if (interrupt == 0)
    {
	ADXL345_writeRegister(ADXL345_REG_INT_MAP, 0x00);
    } else
    {
	ADXL345_writeRegister(ADXL345_REG_INT_MAP, 0xFF);
    }

    ADXL345_writeRegister(ADXL345_REG_INT_ENABLE, 0xFF);
}

activites_t ADXL345_readActivites(void)
{
	if(ADXL345_sens.sensorID!=0){
		uint8_t data = ADXL345_readRegister(ADXL345_REG_INT_SOURCE);

		ADXL345_sens.act.isOverrun = ((data >> ADXL345_OVERRUN) & 1);
		ADXL345_sens.act.isWatermark = ((data >> ADXL345_WATERMARK) & 1);
		ADXL345_sens.act.isFreeFall = ((data >> ADXL345_FREE_FALL) & 1);
		ADXL345_sens.act.isInactivity = ((data >> ADXL345_INACTIVITY) & 1);
		ADXL345_sens.act.isActivity = ((data >> ADXL345_ACTIVITY) & 1);
		ADXL345_sens.act.isDoubleTap = ((data >> ADXL345_DOUBLE_TAP) & 1);
		ADXL345_sens.act.isTap = ((data >> ADXL345_SINGLE_TAP) & 1);
		ADXL345_sens.act.isDataReady = ((data >> ADXL345_DATA_READY) & 1);

		data = ADXL345_readRegister(ADXL345_REG_ACT_TAP_STATUS);

		ADXL345_sens.act.isActivityOnX = ((data >> 6) & 1);
		ADXL345_sens.act.isActivityOnY = ((data >> 5) & 1);
		ADXL345_sens.act.isActivityOnZ = ((data >> 4) & 1);
		ADXL345_sens.act.isTapOnX = ((data >> 2) & 1);
		ADXL345_sens.act.isTapOnY = ((data >> 1) & 1);
		ADXL345_sens.act.isTapOnZ = ((data >> 0) & 1);
	}
    return ADXL345_sens.act;
}

// Write byte to register

// Read byte to register

// Read byte from register

// Read word from register


void ADXL345_writeRegisterBit(uint8_t reg, uint8_t pos, bool state)
{
    uint8_t value;
    value = ADXL345_readRegister(reg);

    if (state)
    {
	value |= (1 << pos);
    } else
    {
	value &= ~(1 << pos);
    }

    ADXL345_writeRegister(reg, value);
}

bool ADXL345_readRegisterBit(uint8_t reg, uint8_t pos)
{
    uint8_t value;
    value = ADXL345_readRegister(reg);
    return ((value >> pos) & 1);
}


bool ADXL345_check_threshold(void){
	  //uint32_t timer =L3G42xx_sens.timer;
	  float delta;
	  delta= ADXL345_sens.acc_z_old-ADXL345_sens.acc_z;

	  ADXL345_sens.acc_z_old=ADXL345_sens.acc_z;
	  if(abs(delta) >  ADXL345_sens.acc_th){
	      return (true);
	  }

      if(abs(ADXL345_sens.roll) >  ADXL345_sens.roll_th){
    	  return (true);
      }

      if(abs( ADXL345_sens.pitch)  >  ADXL345_sens.pitch_th){
    	  return (true);
      }

      if(abs( ADXL345_sens.yaw) >  ADXL345_sens.yaw_th){
    	  return (false);
      }else{
          return (false);
      }
	  // Wait to full timeStep period
	  //delay((timeStep*1000) - (L3G42xx_sens.timer - timer));
}























void ADXL345_displaySensorDetails(void)
{


  printf("------------------------------------\n\r");
  printf  ("Sensor: %s \n\r",ADXL345_sens.name);
  printf  ("Driver Ver: %d  \n\r",ADXL345_sens.version);
  printf  ("Unique ID:  %ld  \n\r",ADXL345_sens.sensorID);
  printf  ("Max Value:  %f   [m/s^2] \n\r",ADXL345_sens.max_value);
  printf  ("Min Value:  %f   [m/s^2] \n\r",ADXL345_sens.min_value);
  printf  ("Resolution: %f   [m/s^2] \n\r",ADXL345_sens.resolution);
  printf("------------------------------------\n\r");
  //delay(500);
}

void ADXL345_displayDataRate(void)
{
  printf  ("Data Rate:    ");

  switch(ADXL345_getDataRate())
  {
    case ADXL345_DATARATE_3200_HZ:
      printf  ("3200 ");
      break;
    case ADXL345_DATARATE_1600_HZ:
      printf  ("1600 ");
      break;
    case ADXL345_DATARATE_800_HZ:
      printf  ("800 ");
      break;
    case ADXL345_DATARATE_400_HZ:
      printf  ("400 ");
      break;
    case ADXL345_DATARATE_200_HZ:
      printf  ("200 ");
      break;
    case ADXL345_DATARATE_100_HZ:
      printf  ("100 ");
      break;
    case ADXL345_DATARATE_50_HZ:
      printf  ("50 ");
      break;
    case ADXL345_DATARATE_25_HZ:
      printf  ("25 ");
      break;
    case ADXL345_DATARATE_12_5_HZ:
      printf  ("12.5 ");
      break;
    case ADXL345_DATARATE_6_25HZ:
      printf  ("6.25 ");
      break;
    case ADXL345_DATARATE_3_13_HZ:
      printf  ("3.13 ");
      break;
    case ADXL345_DATARATE_1_56_HZ:
      printf  ("1.56 ");
      break;
    case ADXL345_DATARATE_0_78_HZ:
      printf  ("0.78 ");
      break;
    case ADXL345_DATARATE_0_39_HZ:
      printf  ("0.39 ");
      break;
    case ADXL345_DATARATE_0_20_HZ:
      printf  ("0.20 ");
      break;
    case ADXL345_DATARATE_0_10_HZ:
      printf  ("0.10 ");
      break;
    default:
      printf  ("???? ");
      break;
  }
  printf(" [Hz] \n\r");
}

void ADXL345_displayRange(void)
{
  printf  ("Range:  +/- ");

  switch(ADXL345_getRange())
  {
    case ADXL345_RANGE_16_G:
      printf  ("16 ");
      break;
    case ADXL345_RANGE_8_G:
      printf  ("8 ");
      break;
    case ADXL345_RANGE_4_G:
      printf  ("4 ");
      break;
    case ADXL345_RANGE_2_G:
      printf  ("2 ");
      break;
    default:
      printf  ("?? ");
      break;
  }
  printf(" [g]\n\r");
}


