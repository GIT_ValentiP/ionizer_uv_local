/*
L3G4200D.cpp - Class file for the L3G4200D Triple Axis Gyroscope Arduino Library.

Version: 1.3.3
(c) 2014 Korneliusz Jarzebski
www.jarzebski.pl

This program is free software: you can redistribute it and/or modify
it under the terms of the version 3 GNU General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "globals.h"
#include <math.h>
#include "i2c.h"
#include <L3G4200D.h>
#include "iwdg.h"

void L3G4200D_init(void){


	// Pitch, Roll and Yaw values
	L3G42xx_sens.pitch = 0;
	L3G42xx_sens.roll = 0;
	L3G42xx_sens.yaw = 0;

	L3G42xx_sens.roll_th  = 200.0f;
	L3G42xx_sens.pitch_th = 15.0f;
	L3G42xx_sens.yaw_th   = 15.0f;

	L3G42xx_sens.delta=2.0f;

	L3G42xx_sens.old.XAxis =0;
	L3G42xx_sens.old.YAxis =0;
	L3G42xx_sens.old.ZAxis =0;
                        //L3G4200D_SCALE_2000DPS   //L3G4200D_SCALE_500DPS   //L3G4200D_DATARATE_400HZ_50 //L3G4200D_DATARATE_100HZ_12_5
	if (L3G4200D_begin(L3G4200D_SCALE_500DPS, L3G4200D_DATARATE_100HZ_12_5)==false){
        printf("L3G4200D gyroscope NOT INITIALIZED \n\r");
	}
//	else{
//		// Calibrate gyroscope. The calibration must be at rest.
//	    L3G4200D_calibrate(100);
//	}
    L3G42xx_sens.timer = 0;
	L3G42xx_sens.timeStep = 0.01;//10ms// 0.1;//100ms //sec

}


bool L3G4200D_begin(l3g4200d_dps_t scale, l3g4200d_odrbw_t odrbw)
{

	//l3g4200d_dps_t scale = L3G4200D_SCALE_2000DPS;
	//l3g4200d_odrbw_t odrbw = L3G4200D_DATARATE_100HZ_12_5;

    // Reset calibrate values
	L3G42xx_sens.d.XAxis = 0;
	L3G42xx_sens.d.YAxis = 0;
	L3G42xx_sens.d.ZAxis = 0;
	L3G42xx_sens.useCalibrate = false;

    // Reset threshold values
    L3G42xx_sens.t.XAxis = .5f;
    L3G42xx_sens.t.YAxis = .5f;
    L3G42xx_sens.t.ZAxis = .5f;
    L3G42xx_sens.actualThreshold = 1;//0.25f;//.5f;

    //Wire.begin();

    // Check L3G4200D Who Am I Register
    if (L3G4200D_fastRegister8(L3G4200D_REG_WHO_AM_I) != 0xD3)
    {
	return false;
    }

    // Enable all axis and setup normal mode + Output Data Range & Bandwidth
    uint8_t reg1 = 0x00;
    reg1 |= 0x0F; // Enable all axis and setup normal mode
    reg1 |= (odrbw << 4); // Set output data rate & bandwidh
    L3G4200D_writeRegister8(L3G4200D_REG_CTRL_REG1, reg1);

    // Disable high pass filter
    //L3G4200D_writeRegister8(L3G4200D_REG_CTRL_REG2, 0x00);//default
    L3G4200D_writeRegister8(L3G4200D_REG_CTRL_REG2, 0x00);

    // Generata data ready interrupt on INT2
    L3G4200D_writeRegister8(L3G4200D_REG_CTRL_REG3, 0x08);

    // Set full scale selection in continous mode
    L3G4200D_writeRegister8(L3G4200D_REG_CTRL_REG4,(scale << 4));//);

    switch(scale)
    {
	case L3G4200D_SCALE_250DPS:
		L3G42xx_sens.dpsPerDigit = .00875f;
	    break;
	case L3G4200D_SCALE_500DPS:
		L3G42xx_sens.dpsPerDigit = .0175f;
	    break;
	case L3G4200D_SCALE_2000DPS:
		L3G42xx_sens.dpsPerDigit = .07f;
	    break;
	default:
	    break;
    }

    // Boot in normal mode, disable FIFO, HPF disabled
    //L3G4200D_writeRegister8(L3G4200D_REG_CTRL_REG5, 0x00);
    L3G4200D_writeRegister8(L3G4200D_REG_CTRL_REG5, 0x00);

    // Calibrate gyroscope. The calibration must be at rest.
    // If you don't want calibrate, comment this line.
    L3G4200D_calibrate(100);
    return true;
}

// Get current scale
l3g4200d_dps_t L3G4200D_getScale(void)
{
    return (l3g4200d_dps_t)((L3G4200D_readRegister8(L3G4200D_REG_CTRL_REG4) >> 4) & 0x03);
}


// Get current output data range and bandwidth
l3g4200d_odrbw_t L3G4200D_getOdrBw(void)
{
    return (l3g4200d_odrbw_t)((L3G4200D_readRegister8(L3G4200D_REG_CTRL_REG1) >> 4) & 0x0F);
}

// Calibrate algorithm
void L3G4200D_calibrate(uint8_t samples)
{
    // Set calibrate
	L3G42xx_sens.useCalibrate = true;

    // Reset values
    float sumX = 0;
    float sumY = 0;
    float sumZ = 0;
    float sigmaX = 0;
    float sigmaY = 0;
    float sigmaZ = 0;

    // Read n-samples
    for (uint8_t i = 0; i < samples; ++i)
    {
		L3G4200D_readRaw();
		sumX += L3G42xx_sens.r.XAxis;
		sumY += L3G42xx_sens.r.YAxis;
		sumZ += L3G42xx_sens.r.ZAxis;
	
		sigmaX += L3G42xx_sens.r.XAxis * L3G42xx_sens.r.XAxis;
		sigmaY += L3G42xx_sens.r.YAxis * L3G42xx_sens.r.YAxis;
		sigmaZ += L3G42xx_sens.r.ZAxis * L3G42xx_sens.r.ZAxis;
		HAL_IWDG_Refresh(&hiwdg);
		//delay(5);
		HAL_Delay(10);
    }

    // Calculate delta vectors
    L3G42xx_sens.d.XAxis = sumX / samples;
    L3G42xx_sens.d.YAxis = sumY / samples;
    L3G42xx_sens.d.ZAxis = sumZ / samples;

    // Calculate threshold vectors
    L3G42xx_sens.thresholdX = sqrt((sigmaX / samples) - (L3G42xx_sens.d.XAxis * L3G42xx_sens.d.XAxis));
    L3G42xx_sens.thresholdY = sqrt((sigmaY / samples) - (L3G42xx_sens.d.YAxis * L3G42xx_sens.d.YAxis));
    L3G42xx_sens.thresholdZ = sqrt((sigmaZ / samples) - (L3G42xx_sens.d.ZAxis * L3G42xx_sens.d.ZAxis));

    // If already set threshold, recalculate threshold vectors
    if (L3G42xx_sens.actualThreshold > 0)
    {
    	printf("L3G4200D threshold set\n\r");
    	//L3G4200D_setThreshold(L3G42xx_sens.actualThreshold);
    }
}

// Get current threshold value
uint8_t L3G4200D_getThreshold(void)
{
    return L3G42xx_sens.actualThreshold;
}

// Set treshold value
void L3G4200D_setThreshold(float multiple)
{
    if (multiple > 0)
    {
		// If not calibrated, need calibrate
		if (!L3G42xx_sens.useCalibrate)
		{
			L3G4200D_calibrate(50);
		}

		// Calculate threshold vectors
		L3G42xx_sens.t.XAxis = L3G42xx_sens.thresholdX * multiple;
		L3G42xx_sens.t.YAxis = L3G42xx_sens.thresholdY * multiple;
		L3G42xx_sens.t.ZAxis = L3G42xx_sens.thresholdZ * multiple;


    } else
    {
		// No threshold
    	L3G42xx_sens.t.XAxis = 0;
    	L3G42xx_sens.t.YAxis = 0;
    	L3G42xx_sens.t.ZAxis = 0;
    }

    printf("X-ths %f \n\r",L3G42xx_sens.t.XAxis);
    printf("Y-ths %f \n\r",L3G42xx_sens.t.YAxis);
    printf("Z-ths %f \n\r",L3G42xx_sens.t.ZAxis);
    // Remember old threshold value
    //L3G42xx_sens.actualThreshold = multiple;
}

// Write 8-bit to register
void L3G4200D_writeRegister8(uint8_t reg, uint8_t value)
{
	 //Create a temporary buffer
	 uint8_t buff[2];

	 //Load the register address and 8-bit data
	 buff[0] = reg;
	 buff[1] = value;

	     //Write the data
   // Wire.beginTransmission(L3G4200D_ADDRESS);
	 HAL_I2C_Master_Transmit(&hi2c1,L3G4200D_ADDRESS<<1,(uint8_t *)buff,2,1000);

	//Wire.send(reg);
	//Wire.send(value);

    //Wire.endTransmission();
}

// Fast read 8-bit from register
uint8_t L3G4200D_fastRegister8(uint8_t reg)
{

	uint8_t value[1];
	uint16_t adr_rd=(L3G4200D_ADDRESS<<1);
	adr_rd =(adr_rd|0x0001);
	//Wire.beginTransmission(L3G4200D_ADDRESS);
	value[0]=reg;

	HAL_I2C_Master_Transmit(&hi2c1,L3G4200D_ADDRESS<<1,(uint8_t *)value,1,1000);//Wire.send(reg);

	HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)value,1,1000);//Wire.requestFrom(L3G4200D_ADDRESS, 1);

	return value[0];
}

// Read 8-bit from register
uint8_t L3G4200D_readRegister8(uint8_t reg)
{
	uint8_t value[1];
	uint16_t adr_rd=(L3G4200D_ADDRESS<<1);
	adr_rd =(adr_rd|0x0001);
	//Wire.beginTransmission(L3G4200D_ADDRESS);
	value[0]=reg;

	HAL_I2C_Master_Transmit(&hi2c1,L3G4200D_ADDRESS<<1,(uint8_t *)value,1,1000);//Wire.send(reg);

	HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)value,1,1000);//Wire.requestFrom(L3G4200D_ADDRESS, 1);

    return value[0];
}

// L3G4200D Temperature sensor output change vs temperature: -1digit/degrCelsius (data representation: 2's complement).
// Value represents difference respect to a reference not specified value.
// So temperature sensor can be used to measure temperature variations: temperarture sensor isn't suitable to return absolute temperatures measures.
// If you run two sequential measures and differentiate them you can get temperature variation.
// This also means that two devices in the same temp conditions can return different outputs.
// Finally, you can use this info to compensate drifts due to temperature changes.
uint8_t L3G4200D_readTemperature(void)
{
    return L3G4200D_readRegister8(L3G4200D_REG_OUT_TEMP);
}

// Read raw values
Vector L3G4200D_readRaw()
{
	uint8_t value[6];
	uint8_t xla=0;// = Wire.receive();
	uint8_t xha=0;// = Wire.receive();
	uint8_t yla=0;// = Wire.receive();
	uint8_t yha=0;// = Wire.receive();
	uint8_t zla=0;// = Wire.receive();
	uint8_t zha=0;// = Wire.receive();

	uint16_t adr_rd=(L3G4200D_ADDRESS<<1);//Wire.beginTransmission(L3G4200D_ADDRESS);
	adr_rd =(adr_rd|0x0001);

	value[0]=(L3G4200D_REG_OUT_X_L | (1 << 7));//Wire.send(L3G4200D_REG_OUT_X_L | (1 << 7));
	HAL_I2C_Master_Transmit(&hi2c1,L3G4200D_ADDRESS<<1,(uint8_t *)value,1,1000);//Wire.send(reg);
	HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)value,6,1000); //Wire.requestFrom(L3G4200D_ADDRESS, 6);
	xla=value[0];// = Wire.receive();
	xha=value[1];// = Wire.receive();
	yla=value[2];// = Wire.receive();
	yha=value[3];// = Wire.receive();
	zla=value[4];// = Wire.receive();
	zha=value[5];// = Wire.receive();


//	value[0]=(L3G4200D_REG_OUT_X_L);//Wire.send(L3G4200D_REG_OUT_X_L | (1 << 7));
//	HAL_I2C_Master_Transmit(&hi2c1,L3G4200D_ADDRESS<<1,(uint8_t *)value,1,1000);
//	HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)value,1,1000);
//	xla=value[0];//
//	value[0]=(L3G4200D_REG_OUT_X_H);//Wire.send(L3G4200D_REG_OUT_X_L | (1 << 7));
//	HAL_I2C_Master_Transmit(&hi2c1,L3G4200D_ADDRESS<<1,(uint8_t *)value,1,1000);
//	HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)value,1,1000);
//	xha=value[0];//
//	value[0]=(L3G4200D_REG_OUT_Y_L);//Wire.send(L3G4200D_REG_OUT_X_L | (1 << 7));
//	HAL_I2C_Master_Transmit(&hi2c1,L3G4200D_ADDRESS<<1,(uint8_t *)value,1,1000);
//	HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)value,1,1000);
//	yla=value[0];//
//	value[0]=(L3G4200D_REG_OUT_Y_H);//Wire.send(L3G4200D_REG_OUT_X_L | (1 << 7));
//	HAL_I2C_Master_Transmit(&hi2c1,L3G4200D_ADDRESS<<1,(uint8_t *)value,1,1000);
//	HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)value,1,1000);
//	yha=value[0];//
//	value[0]=(L3G4200D_REG_OUT_Z_L);//Wire.send(L3G4200D_REG_OUT_X_L | (1 << 7));
//	HAL_I2C_Master_Transmit(&hi2c1,L3G4200D_ADDRESS<<1,(uint8_t *)value,1,1000);
//	HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)value,1,1000);
//	zla=value[0];//
//	value[0]=(L3G4200D_REG_OUT_Z_H);//Wire.send(L3G4200D_REG_OUT_X_L | (1 << 7));
//	HAL_I2C_Master_Transmit(&hi2c1,L3G4200D_ADDRESS<<1,(uint8_t *)value,1,1000);
//	HAL_I2C_Master_Receive(&hi2c1,adr_rd,(uint8_t *)value,1,1000);
//	zha=value[0];//

	L3G42xx_sens.r.XAxis = (int16_t)(xha << 8 | xla);
	L3G42xx_sens.r.YAxis = (int16_t)(yha << 8 | yla);
	L3G42xx_sens.r.ZAxis = (int16_t)(zha << 8 | zla);

    return L3G42xx_sens.r;
}

// Read normalized values
Vector L3G4200D_readNormalize()
{

//	float  fXg, fYg, fZg;

	L3G4200D_readRaw();



    if (L3G42xx_sens.useCalibrate)
    {
//      fXg =  (L3G42xx_sens.r.XAxis - L3G42xx_sens.d.XAxis) * L3G42xx_sens.dpsPerDigit;
//		fYg =  (L3G42xx_sens.r.YAxis - L3G42xx_sens.d.YAxis) * L3G42xx_sens.dpsPerDigit;
//		fZg =  (L3G42xx_sens.r.ZAxis - L3G42xx_sens.d.ZAxis) * L3G42xx_sens.dpsPerDigit;
//
//
//		L3G42xx_sens.n.XAxis = fXg * ALPHA_G + (L3G42xx_sens.old.XAxis * (1.0-ALPHA_G));
//		L3G42xx_sens.old.XAxis = L3G42xx_sens.n.XAxis ;
//
//		L3G42xx_sens.n.YAxis = fYg * ALPHA_G + (L3G42xx_sens.old.YAxis  * (1.0-ALPHA_G));
//		L3G42xx_sens.old.YAxis  = L3G42xx_sens.n.YAxis;
//
//		L3G42xx_sens.n.ZAxis= fZg * ALPHA_G + (L3G42xx_sens.old.ZAxis  * (1.0-ALPHA_G));
//		L3G42xx_sens.old.ZAxis = L3G42xx_sens.n.ZAxis;

    	L3G42xx_sens.n.XAxis = (L3G42xx_sens.r.XAxis - L3G42xx_sens.d.XAxis) * L3G42xx_sens.dpsPerDigit;
    	L3G42xx_sens.n.YAxis = (L3G42xx_sens.r.YAxis - L3G42xx_sens.d.YAxis) * L3G42xx_sens.dpsPerDigit;
    	L3G42xx_sens.n.ZAxis = (L3G42xx_sens.r.ZAxis - L3G42xx_sens.d.ZAxis) * L3G42xx_sens.dpsPerDigit;


    } else{
    	L3G42xx_sens.n.XAxis = L3G42xx_sens.r.XAxis * L3G42xx_sens.dpsPerDigit;
    	L3G42xx_sens.n.YAxis = L3G42xx_sens.r.YAxis * L3G42xx_sens.dpsPerDigit;
    	L3G42xx_sens.n.ZAxis = L3G42xx_sens.r.ZAxis * L3G42xx_sens.dpsPerDigit;
    }


    if (L3G42xx_sens.actualThreshold > 0)
    {
	if (abs(L3G42xx_sens.n.XAxis) < L3G42xx_sens.t.XAxis) L3G42xx_sens.n.XAxis = 0;
	if (abs(L3G42xx_sens.n.YAxis) < L3G42xx_sens.t.YAxis) L3G42xx_sens.n.YAxis = 0;
	if (abs(L3G42xx_sens.n.ZAxis) < L3G42xx_sens.t.ZAxis) L3G42xx_sens.n.ZAxis = 0;
    }

    return L3G42xx_sens.n;
}

void L3G4200D_update(void){
	  //uint32_t timer =L3G42xx_sens.timer;

	  // Read normalized values
	  L3G4200D_readNormalize();

	  // Calculate Pitch, Roll and Yaw
	  L3G42xx_sens.timeStep=(L3G42xx_sens.timer);
	  L3G42xx_sens.timeStep=(L3G42xx_sens.timeStep/1000);
	  L3G42xx_sens.pitch += (L3G42xx_sens.n.YAxis * L3G42xx_sens.timeStep);
	  L3G42xx_sens.roll  += (L3G42xx_sens.n.XAxis * L3G42xx_sens.timeStep);
	  L3G42xx_sens.yaw   += (L3G42xx_sens.n.ZAxis * L3G42xx_sens.timeStep);
	  L3G42xx_sens.timer  = 0;

	  L3G42xx_sens.temperature= L3G4200D_readTemperature();

	  // Wait to full timeStep period
	  //delay((timeStep*1000) - (L3G42xx_sens.timer - timer));
}

bool L3G4200D_check_threshold(void){
	  //uint32_t timer =L3G42xx_sens.timer;
      if(abs(L3G42xx_sens.pitch) > L3G42xx_sens.pitch_th){
    	  return (true);
      }
      if(abs(L3G42xx_sens.yaw) > L3G42xx_sens.yaw_th){
    	  return (true);
      }
      if(abs(L3G42xx_sens.roll) > L3G42xx_sens.roll_th){
    	  return (false);
      }else{
          return (false);
      }
	  // Wait to full timeStep period
	  //delay((timeStep*1000) - (L3G42xx_sens.timer - timer));
}


bool L3G4200D_check_hysteresis(void){
	  //uint32_t timer =L3G42xx_sens.timer;
      if(abs(L3G42xx_sens.pitch) < (L3G42xx_sens.pitch_th-L3G42xx_sens.delta)){
    	  return (true);
      }
      if(abs(L3G42xx_sens.yaw) < L3G42xx_sens.yaw_th-L3G42xx_sens.delta){
    	  return (true);
      }
//      if(abs(L3G42xx_sens.roll) < (L3G42xx_sens.roll_th/2)){
//    	  return (true);
//      }
      else{
          return (false);
      }
	  // Wait to full timeStep period
	  //delay((timeStep*1000) - (L3G42xx_sens.timer - timer));
}
