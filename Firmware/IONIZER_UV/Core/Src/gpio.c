/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"
/* USER CODE BEGIN 0 */
#include "globals.h"
#include <L3G4200D.h>
#include "ADXL345.h"
#include "tim.h"
/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
        * Free pins are configured automatically as Analog (this feature is enabled through
        * the Code Generation settings)
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, COIL1_ORANGE_Pin|COIL2_YELLOW_Pin|COIL3_PINK_Pin|LED_END_SENSOR_HALL_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, COIL4_BLUE_Pin|LED_STATUS_Pin|LED_UV_ON_OFF_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LED4_SERVICE_Pin|LED_POWER_Pin|LED3_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = BTN_PROGRAM_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;//GPIO_MODE_IT_RISING;//
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BTN_PROGRAM_GPIO_Port, &GPIO_InitStruct);
  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
  /*Configure GPIO pins : PCPin PCPin PCPin PCPin
                           PCPin */
  GPIO_InitStruct.Pin = HALL1_STOP_Pin|HALL2_STOP_Pin|IN3_SERVICE_Pin|IN2_SERVICE_Pin
                          |ACC_IN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PAPin PAPin PAPin PAPin */
  GPIO_InitStruct.Pin = COIL1_ORANGE_Pin|COIL2_YELLOW_Pin|COIL3_PINK_Pin|LED_END_SENSOR_HALL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PBPin PBPin PBPin */
  GPIO_InitStruct.Pin = COIL4_BLUE_Pin|LED_STATUS_Pin|LED_UV_ON_OFF_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = IN1_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(IN1_SERVICE_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PCPin PCPin PCPin */
  GPIO_InitStruct.Pin = LED4_SERVICE_Pin|LED_POWER_Pin|LED3_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = BTN_START_STOP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;//GPIO_NOPULL;
  HAL_GPIO_Init(BTN_START_STOP_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 2 */
void set_led(uint8_t nled,GPIO_PinState status){

	switch(nled){
	      case(LED_STATUS):
		          HAL_GPIO_WritePin(GPIOB,LED_STATUS_Pin, status);
	    		  break;
	      case(LED_POWER):
		          HAL_GPIO_WritePin(GPIOC, LED_POWER_Pin, status);
				  break;
	      case(LED_SENSOR_HALL):
		          HAL_GPIO_WritePin(GPIOA, LED_END_SENSOR_HALL_Pin, status);
				  break;
	      case(POWER_ON_OFF):
		          HAL_GPIO_WritePin(GPIOC, LED3_SERVICE_Pin, status);
				  break;
	      case(LED_UV):
		          HAL_GPIO_WritePin(GPIOB,LED_UV_ON_OFF_Pin, status);
				  break;
	      case(STEP_UP_CTRL):
		          HAL_GPIO_WritePin(LED4_SERVICE_GPIO_Port, LED4_SERVICE_Pin,status);//0:pressed 1:released
				  break;
	      default:
					HAL_GPIO_WritePin(GPIOB,LED_STATUS_Pin, status);

					HAL_GPIO_WritePin(GPIOC, LED_POWER_Pin, status);

					HAL_GPIO_WritePin(GPIOA, LED_END_SENSOR_HALL_Pin, status);

					//HAL_GPIO_WritePin(GPIOC, LED3_SERVICE_Pin, status);

					HAL_GPIO_WritePin(GPIOB,LED_UV_ON_OFF_Pin, status);
	    	      break;
	}
}


void set_led_rgb(uint8_t nled,uint16_t red,uint16_t green,uint16_t blue){

	//uint32_t period;
	uint32_t duty;
	switch(nled){
	     case(LED_START_STOP):

						duty=htim2.Init.Period*red;
		                htim2.Instance->CCR1=(duty/100);
						duty=htim2.Init.Period*green;
						htim2.Instance->CCR3=(duty/100);
						duty=htim2.Init.Period*blue;
						htim2.Instance->CCR4=(duty/100);
					  break;
	      case(LED_PROGRAM):

					duty=htim1.Init.Period*red;
	                htim1.Instance->CCR3=(duty/100);
					duty=htim1.Init.Period*green;
					htim1.Instance->CCR2=(duty/100);
					duty=htim1.Init.Period*blue;
					htim1.Instance->CCR1=(duty/100);
				  break;

         case(LED_MANUAL)://

					duty=htim3.Init.Period*red;
					htim3.Instance->CCR1=(duty/100);
					duty=htim3.Init.Period*green;
					htim3.Instance->CCR2=(duty/100);
					duty=htim3.Init.Period*blue;
					htim3.Instance->CCR4=(duty/100);//original
					//htim2.Instance->CCR2=(duty/100);//PWM1_SERVICE
	    		  break;
	      default:

	    	      break;
	}
}
void refresh_led_rgb(void){
uint8_t id;
for(id=0;id<2;id++){
	if(rgb[id].config.f.fade==1){
			if(rgb[id].period_fading==0){
				if(rgb[id].config.f.up_down==0){
					if (rgb[id].duty_fading <FADE_MAX_LEVEL){//100
						rgb[id].duty_fading++;
					}else{
						   rgb[id].config.f.up_down=1;
						 }
				}else if (rgb[id].duty_fading >FADE_MIN_LEVEL){
							rgb[id].duty_fading--;
					  }else{
							rgb[id].config.f.up_down=0;
						   }
				rgb[id].period_fading=PERIOD_FADE_RGB;
				set_led_rgb(id,rgb[id].duty_fading,rgb[id].duty_fading,rgb[id].duty_fading);
				//set_led_rgb(id,0,rgb[id].duty_fading,0);
			}
		}else if(rgb[id].config.f.refresh==1){
				   set_led_rgb(id,rgb[id].red,rgb[id].green,rgb[id].blue);
				   rgb[id].config.f.refresh=0;
			  }else if(rgb[id].config.f.blink==1){
				       if(ionizer1.task!=IONIZE_PROCESS_TASK){
							if(TOGGLE_PIN_LED==1){
								set_led_rgb(id,rgb[id].red,rgb[id].green,rgb[id].blue);
							}else{
								set_led_rgb(id,0,0,0);
							}
				       }else{
				    	    if(TOGGLE_PIN_LED==1){
								set_led_rgb(LED_START_STOP,rgb[LED_START_STOP].red,rgb[LED_START_STOP].green,rgb[LED_START_STOP].blue);
								set_led_rgb(LED_PROGRAM,0,0,0);
							}else{
								set_led_rgb(LED_PROGRAM,rgb[LED_PROGRAM].red,rgb[LED_PROGRAM].green,rgb[LED_PROGRAM].blue);
								set_led_rgb(LED_START_STOP,0,0,0);
							}
				       }
					}
 }
}


void set_led_level(uint8_t id){
	 switch(read_keyboard_event){
			   case(0):

			            rgb[id].config.all=0;
					    rgb[id].config.f.refresh=1;
					    rgb[id].red=100;
					    rgb[id].green=0;
					    rgb[id].blue=0;
						break;
			   case(1):

			            rgb[id].config.all=0;
						rgb[id].config.f.refresh=1;
						rgb[id].red=RED_LEV_1;
						rgb[id].green=GREEN_LEV_1;
						rgb[id].blue=BLUE_LEV_1;
						break;
			   case(2):

			            rgb[id].config.all=0;
						rgb[id].config.f.refresh=1;
						rgb[id].red=RED_LEV_2;
						rgb[id].green=GREEN_LEV_2;
						rgb[id].blue=BLUE_LEV_2;
						break;
			   case(3):

			            rgb[id].config.all=0;
						rgb[id].config.f.refresh=1;
						rgb[id].red=RED_LEV_3;
						rgb[id].green=GREEN_LEV_3;
						rgb[id].blue=BLUE_LEV_3;
						break;
			   default:

						break;
			 }


}

void kick_power_switch(uint8_t status){
	switch(status){
	   case(0):
			   if(keys.btn.pwroff_sysup==1){
				   set_led(POWER_ON_OFF,LED_OFF);
			   }
			   break;
	   case(1):
		       set_led(POWER_ON_OFF,LED_ON);
		       keys.btn.pwroff_sysup=1;
		       short_press_time=TIME_POWER_SWITCH_OFF;
			   break;
	   default:
		       break;
	}
}

bool read_sensor(void){
	GPIO_PinState status;
	bool result;
	status= HAL_GPIO_ReadPin(HALL1_STOP_GPIO_Port, HALL1_STOP_Pin);
	sensor_hall.sw.sw1_l=((status==GPIO_PIN_RESET)?0:1);
	status= HAL_GPIO_ReadPin(HALL2_STOP_GPIO_Port, HALL2_STOP_Pin);
	sensor_hall.sw.sw1_r=((status==GPIO_PIN_RESET)?0:1);

	keys_old.wrd=keys.wrd;
	status= HAL_GPIO_ReadPin(BTN_PROGRAM_GPIO_Port, BTN_PROGRAM_Pin);//0:pressed 1:released
	keys.btn.prog =((status==GPIO_PIN_RESET)?1:0);//sensor_hall.sw.sw2_c=

	status= HAL_GPIO_ReadPin(BTN_START_STOP_GPIO_Port, BTN_START_STOP_Pin);//0:pressed 1:released
	keys.btn.start_stop=((status==GPIO_PIN_RESET)?1:0);//sensor_hall.sw.sw3_c=((status==GPIO_PIN_RESET)?1:0);

	status= HAL_GPIO_ReadPin(IN3_SERVICE_GPIO_Port, IN3_SERVICE_Pin);//0:pressed 1:released
    keys.btn.manual=((status==GPIO_PIN_RESET)?1:0);//

    status= HAL_GPIO_ReadPin(ACC_IN_GPIO_Port, ACC_IN_Pin);//0:pressed 1:released
    keys.btn.acc_int=((status==GPIO_PIN_RESET)?1:0);//

    status= HAL_GPIO_ReadPin(IN1_SERVICE_GPIO_Port, IN1_SERVICE_Pin);//0:pressed 1:released
    keys.btn.proxy_int=((status==GPIO_PIN_RESET)?1:0);//

    status= HAL_GPIO_ReadPin(IN2_SERVICE_GPIO_Port, IN2_SERVICE_Pin);//0:pressed 1:released
    keys.btn.docking_in=((status==GPIO_PIN_RESET)?1:0);//

    if((sensor_hall.sw.sw1_r==0)||(sensor_hall.sw.sw1_l==0)){//check sensor hall (right or left) turn on
		set_led(2,GPIO_PIN_SET);//turn on LED Green

//		if((sensor_hall.sw.sw1_l==0)&&(ionizer1.direction==0)){
//			result=true;
//		}else if((sensor_hall.sw.sw1_r==0)&&(ionizer1.direction==1)){
//			     result=true;
//			  }else{
//				 result=false;
//			  }
		  result=true;
	}else{//not reached any end rail
		set_led(2,GPIO_PIN_RESET);
		result=false;
	}

    if((keys.btn.docking_in==1)&&(keys_old.btn.docking_in==0)){
    	keys.btn.detach_docking=1;
    }else if((keys.btn.docking_in==0)&&(keys_old.btn.docking_in==1)){
       	    keys.btn.detach_docking=1;
    }

    if(display_refresh_event==0){//NONE cycle active
      if(keys.btn.start_stop==BTN_NEG_PRESSED){//START Button pressed
    	  if(end_process==false){
    	     display_refresh_event=1;
    	  }
      }else{
    	  end_process=false;
      }
    }else{//cycle ACTIVE and STOP Btn pressed
    	 if(keys.btn.start_stop==BTN_NEG_PRESSED){
    	    display_refresh_event=0;
    	    result=true;
    	    end_process=true;
    	 }
    }
    if(ADXL345_check_threshold()==true){
    		 display_refresh_event=0;
			 result=true;
			 end_process=true;
			 ionizer1.error.bit.b0=1;
    }else{
    	 ionizer1.error.bit.b0=0;
    }
//    if ( ADXL345_sens.act.isActivityOnX) {
//    	 display_refresh_event=0;
//		 result=true;
//		 end_process=true;
//		 ionizer1.error.bit.b1=1;
//    }else{
//    	ionizer1.error.bit.b1=0;
//    }
	return(result);
}
/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
